import { 
    CHANGE_TASK_FORM_VISIBLE, 
    SHOW_DAY_TASKS,
    CHANGE_PROGRESS_FORM_VISIBLE,
    SHOW_DAY_PURPOSES,
    SET_DAYS_WITH_TASK,
} from "./actions";

export default (state = init(), actions) => {
    switch(actions.type) {
        case CHANGE_TASK_FORM_VISIBLE:
            return changeTaskFormVisible(state)
        case SHOW_DAY_TASKS:
            return showDayTasks(state, actions.date, actions.tasks)
        case CHANGE_PROGRESS_FORM_VISIBLE:
            return changeProgressFormVisible(state)
        case SHOW_DAY_PURPOSES:
            return showDayPurposes(state, actions.purposes)
        case SET_DAYS_WITH_TASK:
            return setDaysWithTask(state, actions.days)
        default:
            return state
    }
}

const init = () => {
    return {
        selectedDate: new Date(),
        isTaskFormVisible: false,
        isProgressFormVisible: false,
        tasks: [],
        progress: [],
        daysWithTask: []
    }
}

const changeTaskFormVisible = (state) => {
    return {
        ...state,
        isTaskFormVisible: !state.isTaskFormVisible
    }
}

const showDayTasks = (state, date, tasks) => {
    tasks.sort((item1, item2) => {
        const date1 = `${item1.date} ${item1.hours}:${item1.minutes}:00`
            .replace(/^(\d{2}).(\d{2}).(\d{4})/g, '$3-$2-$1')
        const date2 = `${item2.date} ${item2.hours}:${item2.minutes}:00`
            .replace(/^(\d{2}).(\d{2}).(\d{4})/g, '$3-$2-$1')
        return new Date(date1) - new Date(date2)
    })
    return {
        ...state,
        selectedDate: date,
        tasks
    }
}

const changeProgressFormVisible = (state) => {
    return {
        ...state,
        isProgressFormVisible: !state.isProgressFormVisible
    }
}

const showDayPurposes = (state, purposes) => {
    return {
        ...state,
        isProgressFormVisible: false,
        progress: purposes.slice(0)
    }
}

const setDaysWithTask = (state, days) => {
    return {
        ...state,
        daysWithTask: days.slice(0)
    }
}