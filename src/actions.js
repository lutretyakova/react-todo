export const CHANGE_TASK_FORM_VISIBLE = 'changeTaskFormVisible'

export const SHOW_DAY_TASKS = 'showDayTasks'

export const CHANGE_PROGRESS_FORM_VISIBLE = 'changeProgressFormVisible'

export const SHOW_DAY_PURPOSES = 'showDayPurposes'

export const SET_DAYS_WITH_TASK = 'setDaysWithTask'

export const changeTaskFormVisible = () => ({
    type: CHANGE_TASK_FORM_VISIBLE
})

export const showDayTasks = (date, tasks) => ({
    type: SHOW_DAY_TASKS,
    date,
    tasks
})

export const changeProgressFormVisible = () => ({
    type: CHANGE_PROGRESS_FORM_VISIBLE
})

export const showDayPurposes = (purposes) => ({
    type: SHOW_DAY_PURPOSES,
    purposes
})

export const setDaysWithTask = (days) => ({
    type: SET_DAYS_WITH_TASK,
    days
})