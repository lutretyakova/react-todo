import React from 'react'

const ProgressDayListItem = props => {
    return <div className="progress-day-list-item">
        { props.text } 
        <span className="progress-from-to">{ props.from } / { props.to } </span>
    </div>
}

export default ProgressDayListItem