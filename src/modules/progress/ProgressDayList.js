import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'

import ProgressDayListItem from './ProgressDayListItem';

import { getNumericDate } from '../utils/dateUtils';
import { readPurposes } from '../utils/db';

class ProgressDayList extends Component {
    render() {
        const progressItems = this.props.state.progress.map(
            (purpose, inx) => {
                return <ProgressDayListItem key={`day-listitem-${inx}`} 
                            from={ purpose.date } 
                            to={ purpose.dateEnd } 
                            text={ purpose.purpose }
                        />
            }
        )
        return(
            <div className="progress-day-list">
                { progressItems }
            </div>
        )
    }

    async componentDidMount() {
        const date = this.props.state.selectedDate
        const purposes = await readPurposes(getNumericDate(date))
        
        const { showDayPurposes } = this.props
        showDayPurposes(purposes)
    }
}

export default connect((state) => ({state}), actions)(ProgressDayList)