import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as actions from '../../actions'

class AddProgressButton extends Component {
    render() {
        return (
            <button className="add-progress" onClick={this.handleAddProgressClick}>+</button>
        )
    }

    handleAddProgressClick = () => {
        const { changeProgressFormVisible } = this.props
        changeProgressFormVisible() 
    }
}

export default connect(state => ({state}), actions)(AddProgressButton)