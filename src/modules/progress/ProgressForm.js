import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as actions from '../../actions'
import FormButton from '../buttons/FormButton';
import { getNumericDate } from '../utils/dateUtils';
import { writePurposeForPeriod, readPurposes } from '../utils/db';

const capitalize = (str) => {
    return str.length 
        ? (str[0].toUpperCase() + str.slice(1))
        : ''
}

class ProgressForm extends Component {
    constructor() {
        super()
        this.state= {
            period: 2,
            purpose: ''
        }
    }

    render() {
        return this.getForm()
    }

    getForm() {
        return this.props.state.isProgressFormVisible
            ? <div className="progress-form show">
                <legend className="form-legend">Добавить новую цель</legend>
                <form>
                    <label htmlFor="period">Продолжительность дней</label>
                    <input type="number" min="2" value={ this.state.period } onChange={this.handleChangePeriod}/>
                    <label htmlFor="purpose">Описание</label>
                    <input name="purpose" value={ this.state.purpose } onChange={this.handleChangePurpose}/>
                    <div className="confirm-cansel-pnl">
                        <FormButton text="Отмена" buttonStyleClass="cansel" eventHandler={ this.handleCansel }/>
                        <FormButton text="Добавить" buttonStyleClass="confirm" eventHandler={ this.handleSubmit }/>
                    </div>
                </form>
            </div>
            : <div className="progress-form"></div>
    }

    handleSubmit = async () => {
        if (! this.state.purpose) {
            return
        }
        
        const dateEnd = new Date(this.props.state.selectedDate)
        dateEnd.setDate(dateEnd.getDate() + Number(this.state.period))
        
        const date = getNumericDate(this.props.state.selectedDate)
        const purpose = {
            date,
            dateEnd: getNumericDate(dateEnd),
            purpose: this.state.purpose
        }

        await writePurposeForPeriod(purpose)
        
        const purposes = await readPurposes(date)
        
        const { showDayPurposes } = this.props
        showDayPurposes(purposes)
    }

    handleCansel = () => {
        const { changeProgressFormVisible } = this.props
        changeProgressFormVisible()
    }

    handleChangePurpose = (event) => {
        this.setState({
            purpose: capitalize(event.target.value)
        })
    }

    handleChangePeriod = (event) => {
        this.setState({
            period: event.target.value
        })
    }
}

export default connect((state) => ({state}), actions)(ProgressForm)