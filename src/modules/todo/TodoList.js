import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as actions from '../../actions'

import Task from './Task'
import { readTasksForDay, readDaysWithTask } from '../utils/db';
import { getNumericDate } from '../utils/dateUtils'

class TodoList extends Component {
    render() {
        const taskList = this.props.state.tasks.length
            ? this.props.state.tasks.map((task, inx) => {
                return <Task key={`task-${inx}-${task.type}`} task={ task } />
            })
            : null
        return(
            <div className="todo-list">
                { taskList }
            </div>
        )
    }

    async componentDidMount () {
        const date = this.props.state.selectedDate
        const tasks = await readTasksForDay(getNumericDate(date))

        const { showDayTasks } = this.props
        showDayTasks(date, tasks)

        const days = await readDaysWithTask()
        const { setDaysWithTask } = this.props
        setDaysWithTask(days)
    }
}

export default connect((state) => ({state}), actions)(TodoList)
