import React from 'react'

import { taskTypes } from '../taskConstant'

const Task = (props) => {
    return <div className="task">
        <div className="task-icon" dangerouslySetInnerHTML={{__html: taskTypes[props.task.type]}}></div>
        <div className="task-time">{ props.task.hours } : {props.task.minutes }</div>
        <div className="task-text">{ props.task.text }</div>
    </div>
}

export default Task