import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as actions from '../../actions'
import { getNumericDate } from '../utils/dateUtils';

class AddTaskButton extends Component {
    render() {
        return (
            this.getButtonPanel()
        )
    }

    getButtonPanel() {
        if(this.props.state.isTaskFormVisible === false && 
            getNumericDate(this.props.state.selectedDate) >= getNumericDate(new Date()) ) {
            return <div className="btn-pnl">
                <button className="add-task" onClick={this.handleClick}>
                    Добавить задание
                    </button>
            </div>
        } else {
            return null
        }
    }

    handleClick = () => {
        const { changeTaskFormVisible } = this.props
        changeTaskFormVisible()
    }
}

export default connect((state) => ({ state }), actions)(AddTaskButton)