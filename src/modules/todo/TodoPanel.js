import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as actions from '../../actions'

import './todoList.css'

import AddTaskButton from './AddTaskButton';
import TodoList from './TodoList';
import AddTaskForm from '../taskForm/AddTaskForm';

import { getFullDate } from '../utils/dateUtils';

class TodoPanel extends Component {
    render() {
        const selectedDate = this.props.state.selectedDate
        return (
            <div className="todo-panel">
                <h1 className="task-header">{ `Список дел на ${ getFullDate(selectedDate) }` }</h1>
                <TodoList />
                <AddTaskButton />
                <AddTaskForm />
            </div>
        )
    }
}

export default connect((state) => ({state}), actions)(TodoPanel)