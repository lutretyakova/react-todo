import React, { Component } from 'react'
import './progress/progress.css'

import AddProgressButton from './progress/AddProgressButton';
import ProgressForm from './progress/ProgressForm';
import ProgressDayList from './progress/ProgressDayList';

class ProgressContainer extends Component {
    render() {
        return (
            <div className="progress-container">
                <h3>Мой прогресс</h3>
                <AddProgressButton />
                <ProgressForm />
                <ProgressDayList />
            </div>
        )
    }

}

export default ProgressContainer