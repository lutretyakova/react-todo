import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../../actions'

import './taskForm.css';

import TimeSet from './TimeSet';
import TaskTypeList from './TaskTypeList';

import { getFullDate, getNumericDate } from '../utils/dateUtils';
import { readTasksForDay, writeTaskForDay, readDaysWithTask } from '../utils/db';
import { DEFAULT_TASK } from '../taskConstant';
import FormButton from '../buttons/FormButton';

const capitalize = (str) => {
    return str.length 
        ? (str[0].toUpperCase() + str.slice(1))
        : ''
}

class AddTaskForm extends Component {
    constructor() {
        super()
        this.state = {
            text: ''
        }
    }

    render () {
        return (
            this.getForm()
        )
    }

    getForm() {
        if (this.props.state.isTaskFormVisible) {
            return <div className="add-task-form show">
                <form>
                    <legend className="form-legend">
                        { `Добавить задачу на ${ getFullDate(this.props.state.selectedDate) }` }                    
                    </legend>
                    <label htmlFor="time">Время</label>
                    <TimeSet onChangeHours={ this.handleChangeHours } onChangeMinutes={ this.handleChangeMinutes }/>
                    <label htmlFor="tasktype">Тип задачи</label>
                    <TaskTypeList onChangeTaskType={ this.handleChangeTaskType }/>
                    <label htmlFor="description">Описание</label>
                    <textarea className="description" value={ this.state.text } onChange={ this.handleChangeText }/>
                    <div className="confirm-cansel-pnl">
                        <FormButton text="Отмена" buttonStyleClass="cansel" eventHandler={ this.handleCansel }/>
                        <FormButton text= "Добавить" buttonStyleClass="confirm" eventHandler={ this.handleSubmit }/>
                    </div>
                </form>
            </div>
        } else {
            return <div className="add-task-form">
            </div>
        }
    }

    handleCansel = () => {
        this.setState({
            text: '',
            taskType: DEFAULT_TASK
        })
        const { changeTaskFormVisible } = this.props
        changeTaskFormVisible()
    }

    handleSubmit = async () => {
        if (this.state.text.replace(/^\s+|\s+$/g, '') &&
            this.state.hours && this.state.minutes
        ) {
            const date = this.props.state.selectedDate
            const task = {
                date: getNumericDate(date),
                hours: this.state.hours,
                minutes: this.state.minutes,
                type: this.state.taskType || DEFAULT_TASK,
                text: this.state.text
            }
            await writeTaskForDay(task)
            const tasks = await readTasksForDay(getNumericDate(date))
            const { showDayTasks } = this.props
            showDayTasks(date, tasks)

            const days = await readDaysWithTask()
            const { setDaysWithTask } = this.props
            setDaysWithTask(days)
            
            this.setState({
                text: ''
            })
        }
    }

    handleChangeHours = (hours) => {
        this.setState({
            hours
        })
    }

    handleChangeMinutes = (minutes) => {
        this.setState({
            minutes
        })
    }

    handleChangeText = (event) => {
        this.setState({
            text: capitalize(event.target.value)
        })
    }

    handleChangeTaskType = (taskType) => {
        this.setState({
            taskType
        })
    }
}

export default connect((state) => ({state}), actions)(AddTaskForm)