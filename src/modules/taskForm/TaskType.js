import React from 'react'
import { taskTypes } from '../taskConstant';

const TaskType = props => {
    const styleTask = {
        left: `${ 40 * props.shift }px`
    }
    return <div 
            style={ styleTask }
            className={ `task-type ${props.activeClass }` } 
            data-task={ props.task }
            data-index={ props.inx}
            dangerouslySetInnerHTML={{__html: taskTypes[props.task]}}></div>
}

export default TaskType