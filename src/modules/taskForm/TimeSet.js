import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as actions from '../../actions'

const formatTime = (time) => (('0' + time).slice(-2))

class TimeSet extends Component {
    constructor() {
        super()
        this.state = {
            hours: formatTime(new Date().getHours()),
            minutes: formatTime(new Date().getMinutes())
        }
    }

    render() {
        return (
            <div className="timeset" name="time">
                <div className="back hours-back" onClick={this.hoursBack}>&lsaquo;</div>
                <div className="hours">
                    {this.state.hours}
                </div>
                <div className="next hours-next" onClick={this.hoursNext}>&rsaquo;</div>
                <div className="separator">:</div>
                <div className="back minutes-back" onClick={this.minutesBack}>&lsaquo;</div>
                <div className="minutes">
                    {this.state.minutes}
                </div>
                <div className="next minutes-next" onClick={this.minutesNext}>&rsaquo;</div>
            </div>
        )
    }

    componentDidMount() {
        this.props.onChangeHours(this.state.hours)
        this.props.onChangeMinutes(this.state.minutes)
    }

    hoursBack = () => {
        const hours = parseInt(this.state.hours, 10) - 1 < 0
            ? '23'
            : formatTime(parseInt(this.state.hours, 10) - 1)

        this.setState({
            hours
        })
        this.props.onChangeHours(hours)
    }

    hoursNext = () => {
        const hours = parseInt(this.state.hours, 10) + 1 > 23
            ? '00'
            : formatTime(parseInt(this.state.hours, 10) + 1)

        this.setState({
            hours
        })
        this.props.onChangeHours(hours)
    }

    minutesBack = () => {
        const minutes = parseInt(this.state.minutes, 10) - 1 < 0
            ? '59'
            : formatTime(parseInt(this.state.minutes, 10) - 1)

        this.setState({
            minutes
        })

        this.props.onChangeMinutes(minutes)
    }

    minutesNext = () => {
        const minutes = parseInt(this.state.minutes, 10) + 1 > 59
            ? '00'
            : formatTime(parseInt(this.state.minutes, 10) + 1)
        this.setState({
            minutes
        })
        this.props.onChangeMinutes(minutes)
    }
}

export default connect((state) => ({ state }), actions)(TimeSet)