import React, { Component } from 'react'

import TaskType from './TaskType'
import { taskTypes, MAX_TASK_SCROLL } from '../taskConstant';

class TaskTypeList extends Component {
    constructor() {
        super()
        this.state = {
            activeInx: -1,
            shift: 0
        }
    }

    render() {
        const taskTypesList = Object.keys(taskTypes).map((task, inx) => {
            return <TaskType 
                shift = { this.state.shift }
                activeClass={ 
                    this.state.activeInx === inx
                        ? 'active'
                        : ''
                }
                task={ task } 
                inx={ inx } 
                key={ `task-type-${inx}` } />
        })
        return (
            <div className="task-type-pnl">
                <div className="back" onClick={ this.handleClickBack }>&lsaquo;</div>
                <div name="tasktype" className="task-type-list"
                    onClick={ this.handleClick }>
                    { taskTypesList }
                </div>
                <div className="next" onClick={ this.handleClickNext }>&rsaquo;</div>
            </div>
        )
    }

    handleClick = (event) => {
        if (event.target.getAttribute("data-task")) {
            this.setState({
                activeInx: parseInt(event.target.getAttribute("data-index"), 10)
            })

            this.props.onChangeTaskType(event.target.getAttribute("data-task"))
        }
    }

    handleClickNext = () => {
        if (this.state.shift === MAX_TASK_SCROLL) {
            return
        }

        this.setState({
            shift: this.state.shift - 1
        })
    }

    handleClickBack = () => {
        if (this.state.shift === 0) {
            return
        }

        this.setState({
            shift: this.state.shift + 1
        })
    }
}

export default TaskTypeList