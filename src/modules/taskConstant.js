export const DAILY = '&#128466;'

export const HOSPITAL = '&#127973;'

export const PILLS = '&#128138;'

export const ROCKET = '&#128640;'

export const PLANE = '&#128747;'

export const CAR = '&#128661;'

export const BOOKS = '&#128218;'

export const SPORTS = '&#9977;'

export const GIFT = '&#127873;'

export const SLEEP = '&#128164;'

export const LEAF = '&#127811;'

export const SYRINGE = '&#128137;'

export const BELL = '&#128276;'

export const taskTypes = {
    'daily': DAILY,
    'rocket': ROCKET,
    'car': CAR,
    'books': BOOKS,
    'sports': SPORTS,
    'gift': GIFT,
    'sleep': SLEEP,
    'leaf': LEAF,
    'plane': PLANE,
    'syringe':  SYRINGE,
    'bell': BELL,
    'hospital': HOSPITAL,
    'pills': PILLS
}

export const MAX_TASK_SCROLL = -10

export const DEFAULT_TASK = 'daily'