import React, { Component } from 'react'
import DatePanel from './DatePanel';

class LeftPanel extends Component {
    render() {
        return (
            <div className="left-panel">
                <header>t<span>o</span>d<span>o</span></header>
                <DatePanel />
            </div>
        )
    }
}

export default LeftPanel