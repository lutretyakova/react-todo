import React, { Component } from 'react'
import { getTime, getWeekDay, getDate } from '../utils/dateUtils';

const UPDATE_TIMER_INTERVAL = 5000

class DatePanel extends Component {
    constructor() {
        super()
        this.state = {
            date: new Date()
        }
    }

    componentDidMount() {
        this.timer = setInterval(this.timerTick, UPDATE_TIMER_INTERVAL)
    }

    timerTick = () => {
        this.setState({
            date: new Date()
        })
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render() {
        return (
            <div className="date-panel">
                <div className="time">{ getTime(this.state.date) }</div>
                <div className="weekday">{ getWeekDay(this.state.date) },</div>
                <div className="date">{ getDate(this.state.date) }</div>
            </div>
        )
    }
}

export default DatePanel