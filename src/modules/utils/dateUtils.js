const TIME_FORMAT = { hour: '2-digit', minute: '2-digit' }
const WEEK_FORMAT = { weekday: 'long' }
const DATE_FORMAT = { day: '2-digit', month: 'long' }
const MONTH_FORMAT = { month: 'long' }
const DAYS_SHOW_COUNT = 42

export const DATE_NUMERIC_FORMAT = { year: 'numeric', month: '2-digit', day: '2-digit'}
export const DATE_FULL_FORMAT = { day: '2-digit', month: 'long', year: 'numeric' }

export const getTime = (date) => date.toLocaleString('ru', TIME_FORMAT)

export const getWeekDay = (date) => date.toLocaleString('ru', WEEK_FORMAT)

export const getDate = (date) => date.toLocaleString('ru', DATE_FORMAT)

export const getFullDate = (date) => date.toLocaleString('ru', DATE_FULL_FORMAT)

export const getNumericDate = (date) => date.toLocaleString('ru', DATE_NUMERIC_FORMAT)

export const getMonth = (date) => date.toLocaleString('ru', MONTH_FORMAT)

export const getDateFromString = (dateStr) => new Date(dateStr.replace(/^(\d{2}).(\d{2}).(\d{4})/g, '$3-$2-$1'))

export const getDaysInMonth = (date) => {
    const month = date.getMonth() + 1
    const year = date.getFullYear()

    const numberDaysInMonth = new Date(year, month, 0).getDate()
    const dayWeekFirst = new Date(`${ year }-${ month }-1`).getDay()

    const numberDaysInLastMonth = new Date(year, month - 1, 0).getDate()

    const days = []
    // -1 для не текущего месяца
    for (let day = numberDaysInLastMonth; 
             day > numberDaysInLastMonth - dayWeekFirst + 1; 
             day--) {
        days.unshift(day * -1)
    }

    for (let day = 1; 
        day <= numberDaysInMonth; 
        day++) {
        days.push(day)
    }

    if (days.length <= DAYS_SHOW_COUNT) {
        const additional = DAYS_SHOW_COUNT - days.length + 1
        for (let day = 1; 
            day < additional; 
            day++) {
            days.push(day * -1)
        }
    }

    return days
}

