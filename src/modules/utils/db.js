import { getDateFromString } from "./dateUtils";

export const writeTaskForDay = async (task) => {
    try {
        const db = await openBase()

        return (await writeTaskToBase(db, task))
    } catch (error) {
        console.log(error)
    }
}

export const writePurposeForPeriod = async (purpose) => {
    try {
        const db = await openBase()
        return (await writeProgressToBase(db, purpose))
    } catch (error) {
        console.log(error)
    }
}

export const readTasksForDay = async (date) => {
    try {
        const db = await openBase()
        return (await readTasksFromBase(db, date))
    } catch (error) {
        console.log(error)
    }
}

export const readPurposes = async (date) => {
    try {
        const db = await openBase()
        return (await readPurposesFromBase(db, date))
    } catch (error) {
        console.log(error)
    }
}

export const readDaysWithTask = async () => {
    try {
        const db = await openBase()
        return (await getDaysWithTask(db))
    } catch (error) {
        console.log(error)
    }
}

const openBase = () => {
    return new Promise((resolve, reject) => {
        if (!window.indexedDB) {
            reject('IndexedDB не поддерживается!')
        }

        window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
        window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

        const dbOpenReq = window.indexedDB.open('todo-local', 1)

        dbOpenReq.onerror = (event) => {
            reject(`Произошла ошибка ${event.target.errorCode}`)
        }

        dbOpenReq.onupgradeneeded = (event) => {
            const db = event.target.result
            createDbStruct(db)
        }

        dbOpenReq.onsuccess = (event) => {
            const db = event.target.result
            resolve(db)
        }
    })
}

const createDbStruct = (upgradeDB) => {
    if (!upgradeDB.objectStoreNames.contains('todo')) {
        const objectStore = upgradeDB.createObjectStore('todo', {
            keyPath: 'key',
            autoIncrement: true
        })
        objectStore.createIndex('date', 'date', { unique: false })
    }

    if (!upgradeDB.objectStoreNames.contains('progress')) {
        const objectStore = upgradeDB.createObjectStore('progress', {
            keyPath: 'key',
            autoIncrement: true
        })
        objectStore.createIndex('date', 'date', { unique: false })
    }
}

const writeTaskToBase = async (db, task) => {
    return new Promise(resolve => {
        const tx = db.transaction(['todo'], 'readwrite')
        const store = tx.objectStore('todo')

        tx.oncomplete = () => {
            db.close()
        }

        store.add(task).onsuccess = () => {
            resolve(task)
        }
    })
}

const writeProgressToBase = async (db, purpose) => {
    return new Promise(resolve => {
        const tx = db.transaction(['progress'], 'readwrite')
        const store = tx.objectStore('progress')

        tx.oncomplete = () => {
            db.close()
        }

        store.add(purpose).onsuccess = () => {
            resolve(purpose)
        }
    })
}

const readTasksFromBase = (db, date) => {
    return new Promise(resolve => {
        const tx = db.transaction(['todo'], 'readonly')
        const store = tx.objectStore('todo')

        tx.oncomplete = () => {
            db.close()
        }

        store.index('date').getAll(date).onsuccess = (event) => {
            resolve(event.target.result)
        }
    })
}

const getDaysWithTask = db => {
    return new Promise(resolve => {
        const tx = db.transaction(['todo'], 'readonly')
        const store = tx.objectStore('todo')

        tx.oncomplete = () => {
            db.close()
        }

        store.index('date').getAll().onsuccess = (event) => {
            resolve([...new Set(
                event.target.result.map(item => item.date)
            )])
        }
    })
}

const readPurposesFromBase = (db, date) => {
    return new Promise(resolve => {
        const tx = db.transaction(['progress'], 'readonly')
        const store = tx.objectStore('progress')

        tx.oncomplete = () => {
            db.close()
        }

        store.index('date').getAll().onsuccess = (event) => {
            const progress = event.target.result.filter(item => {
                const dateStart = getDateFromString(item.date)
                const dateEnd = getDateFromString(item.dateEnd)
                const selectedDate = getDateFromString(date)
                return dateStart <= selectedDate && dateEnd >= selectedDate
            })
            resolve(progress)
        }
    })
}