import React from 'react'
import './buttons.css'

const FormButton = props => {
    return <button type="button" 
                className={` btn ${ props.buttonStyleClass}`} 
                onClick={ props.eventHandler }>
            { props.text }
        </button>
}

export default FormButton