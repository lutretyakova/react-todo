import React from 'react'
import CalendarContainer from './CalendarContainer';
import ProgressContainer from './ProgressContainer';

import '../css/rightPanel.css'

const RightPanel = () => {
    return (
        <div className="right-panel">
            <CalendarContainer />
            <ProgressContainer />
        </div>
    )
}

export default RightPanel