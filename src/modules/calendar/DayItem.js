import React, { Component } from 'react'

import { connect } from 'react-redux';
import * as actions from '../../actions'

import { readTasksForDay, readPurposes } from '../utils/db';
import { getNumericDate } from '../utils/dateUtils';

class DayItem extends Component {
    render() {
        const dateInCalendar = getNumericDate(this.getDateInCalendar())
        const currentClass = this.props.date < 0
            ? 'inactive'
            : ''
        const todayClass = dateInCalendar === getNumericDate(new Date())
            ? 'today'
            : ''
        const selectClass = this.props.state.selectedDate !== null && 
            this.props.date > 0 &&
            dateInCalendar === getNumericDate(this.props.state.selectedDate)
                ? 'selected'
                : ''
        const withTaskClass = this.props.state.daysWithTask.includes(dateInCalendar)
                ? dateInCalendar > getNumericDate(new Date())
                    ? 'mark-future'
                    : 'mark-past'
                : ''
        return (
            <div className={`day-item ${currentClass} ${withTaskClass} ${todayClass} ${selectClass}` }
                onClick={this.handleClick}>
                { Math.abs(this.props.date) }
            </div>
        )
    }

    getDateInCalendar() {
        const day = this.props.date
        const month = this.props.viewPeriod.getMonth() + 1
        const year = this.props.viewPeriod.getFullYear()

        const date = new Date(`${year}-${month}-${day}`)
        return date
    }

    handleClick = async () => {
        if (this.props.date < 0) {
            return
        }
        const date = this.getDateInCalendar()

        const tasks = await readTasksForDay(getNumericDate(date))
        const { showDayTasks } = this.props
        showDayTasks(date, tasks)

        const purposes = await readPurposes(getNumericDate(date))
        const { showDayPurposes } = this.props
        showDayPurposes(purposes)
    }
}

export default connect((state) => ({ state }), actions)(DayItem)