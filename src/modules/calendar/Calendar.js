import React, { Component } from 'react'

import './calendar.css'
import { getMonth, getDaysInMonth } from '../utils/dateUtils'
import DayItem from './DayItem';

class Calendar extends Component {
    constructor() {
        super()
        this.state = {
            date: new Date()
        }
    }

    render() {
        const daysInCalendar = getDaysInMonth(this.state.date).map((item) => {
                const key = `dayitem
                    -${ item > 0 
                        ? this.state.date.getMonth()
                        : 'unset' 
                    }
                    -${Math.abs(item)}
                }`
                return <DayItem key={key} date={item} viewPeriod={this.state.date}/>
            }) 
        return (
            <div className="calendar">
                <div className="month-nav previous" onClick={ this.handlePreviousClick }>&lsaquo;</div>
                <div className="month">{ getMonth(this.state.date) }</div>
                <div className="month-nav next" onClick={ this.handleNextClick }>&rsaquo;</div>
                { daysInCalendar }
            </div>
        )
    }

    handlePreviousClick = () => {
        const month = this.state.date.getMonth() - 1
        this.state.date.setMonth(month)
        this.setState({
            date: this.state.date
        })
    }

    handleNextClick = () => {
        const month = this.state.date.getMonth() + 1
        this.state.date.setMonth(month)
        this.setState({
            date: this.state.date
        })
    }
}

export default Calendar