import React from 'react'
import Calendar from './calendar/Calendar';

const CalendarContainer = () => {
    return <div className='calendar-container'>
            <h3>Календарь</h3>
            <Calendar />
        </div>
}

export default CalendarContainer