import React from 'react';
import "./css/Todo.css";

import LeftPanel from "./modules/leftPanel/LeftPanel";
import RightPanel from './modules/RightPanel';
import TodoPanel from './modules/todo/TodoPanel';

const Todo = () => {
    return (
        <div className="container">
            <LeftPanel />
            <TodoPanel />
            <RightPanel />
        </div>
    )
}

export default Todo